#! hy
(require [hypeg.parser [defparser rule]])
(require [hypeg.helpers [handle-choice]])

(defparser Calculator {:debug False}
  ; Expr    ← Sum
  (rule expr (Rule "sum")
    p_results)

  ; Sum     ← Product ([+-] Product)*
  (rule sum (Seq (Rule "product") (KleenStar (Seq (CharIn "+-")(Rule "product"))))
    (setv [first rest] p_results)
    (for [[op second] rest]
      (cond
        [(= op "+")
         (+= first second)]
        [(= op "-")
         (-= first second)]))
    first)

  ; Product ← Value ([*/] Value)*
  (rule product (Seq (Rule "value") (KleenStar (Seq (CharIn "*/")(Rule "value"))))
    (setv [first rest] p_results)
    (for [[op second] rest]
      (cond
        [(= op "*")
         (*= first second)]
        [(= op "/")
         (/= first second)]))
    first)

  ; Value ← Number / '(' Expr ')'
  (rule value (Choice (Rule "number") (Seq "(" (Rule "expr") ")" ))
    (handle-choice p_results
	  it
      (get it 1)))

  ; Number ← [0-9]+
  (rule number (OneOrMore (CharIn "0123456789"))
    (int p_raw)))

(defmain [prog &rest args]
  (setv expr (.join "" (list (map str.strip args))))
  (print ((Calculator) expr)))
