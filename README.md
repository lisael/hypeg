# HyPEG

The best almost pure Python PEG grammar parser generator.

HyPEG is said "almost pure Python" for two reasons:
  1. It does compile to pure Python
    - we aim to provide a code generator that generate parser classes without any
      dependency, other than Python stdlib, at runtime
  2. It should be usable without writting any Hy code, just Python and HyPEG's PEG dialect
    - That's for those who don't get Hy.
    - However...
      - Some optimisations require a deep introspection and a Hygher level of consciousness
      - Hy is much more fun, solid and terse than the same old boring parsing code

## Hy World!

(AKA the obligatory calculator example)

```cl
(require [hypeg.parser [defparser rule]])
(require [hypeg.helpers [handle-choice]])

(defparser Calculator {:debug False}
  ; Expr <- Sum
  (rule expr (Rule "sum")
    p_results)

  ; Sum <- Product ([+-] Product)*
  (rule sum (Seq (Rule "product") (KleenStar (Seq (CharIn "+-")(Rule "product"))))
    (setv [first rest] p_results)
    (for [[op second] rest]
      (cond
        [(= op "+") (+= first second)]
        [(= op "-") (-= first second)]))
    first)

  ; Product <- Value ([*/] Value)*
  (rule product (Seq (Rule "value") (KleenStar (Seq (CharIn "*/")(Rule "value"))))
    (setv [first rest] p_results)
    (for [[op second] rest]
      (cond
        [(= op "*") (*= first second)]
        [(= op "/") (/= first second)]))
    first)

  ; Value <- Number / '(' Expr ')'
  (rule value (Choice (Rule "number") (Seq "(" (Rule "expr") ")" ))
    (handle-choice p_results
	    it
      (get it 1)))

  ; Number <- [0-9]+
  (rule number (OneOrMore (CharIn "0123456789"))
    (int p_raw))
)

(print ((Calculator) "(11+2)*4/2-3"))
```

### Breakdown

#### Boilerplate

The first two lines are boilerplate Hy require statements to include Hypeg macros.

#### Defparser

The macro `defparser` has this signature:

```cl
(defmacro/g! defparser [name opts &rest rules])
```

It generates a Parser class named according to `name`. The rules are then inserted
as methods in the class. Note that the user can define her own methods using the classic
`defn` macro.

`opts` is a dict with parsers options. At the moment, only `:debug` is implemented
that outputs some debug information while parsing.

#### Rules

The `rule` macro:

```cl
(defmacro/g! rule [name opts expr &rest handler])
```

This generates a method that has the `name` of the rule and implements the given PEG
`expr`ession. A second method is created, named `on_<name>` (AKA the rule handler)
containing the `handler` code.

The sharp-eyed reader noticed that in the example code there is no `opts` between the
name of the rule and the expression. In fact, `defparser` injects its own opts if the
rule doesn't have a dict as second parameter.

#### Handler

The rule handler takes 2 arguments, `p_raw` and `p_results`
that are respectively the raw matching string and a nested list of the results
of the rule's expression.

In our example, the `product` rule handler is called with those args:

```python
p_raw = '(11+2)*4/2'
p_results = [13, [['*', 4], ['/', 2]]]
```

This makes sense if you read the expression of the rule 

```cl
  (Seq (Rule "value") (KleenStar (Seq (CharIn "*/")(Rule "value")))
```

The first `13` is the result of the handler of the rule `value` (which happens here
to be an `expr`, which happens in turn to be a `sum`). Then,
we have a list of results of the `KleenStar` (aka *, aka zero-or-more) evaluation.
Each of those nested lists is the result of the `Seq`. A sequence result is a list
of the result of all of its children.

Here's a breakdown of `p_results`:

```python
p_results = [      # start of the main Seq
    13,            #   (Rule "value") 
    [              #   start of the KleenStar
        ["*", 4],  #       (Seq (CharIn "*/" ) (Rule "value")) 
        ["/", 2]   #       (Seq (CharIn "*/" ) (Rule "value")) 
    ]
]
```

The `product` rule handler reads like:

```cl
    (setv [first rest] p_results)
    (for [[op second] rest]
      (cond
        [(= op "*") (*= first second)]
        [(= op "/") (/= first second)]))
    first
```

Basically, the handler code breaks `p_results` to extract interesting parts and
return a usefull value (which may or may not be a string, here it's a number
as `first` is the result of the `value` rule.).

##### Failing in handlers

Although it's not a good practice (as PEG grammars should be able to express any
context free grammar), one may way raise `NoMatch(self.p_pos)` in the handler. The
parsing is then resume as if the rule did not match.

#### Handlers Helpers

HyPEG provides many helpers to help in the results matching process (As of today, there
is no less than 1 –one!– of them.)

##### handle-choice

The `handle-choice` handler helper unpack the result of a choices result list and
call the code associated with the choice that did match. Indeed, `Choice` expr
return a list of result, one per branch. Non-matching branches result is the 
`NoMatch` exception raised while parsing the branch.

In the calculator example, we use this helper for `value` handler  which can be
a number or an expression:

```cl
; Value ← Number / '(' Expr ')'
(rule value (Choice (Rule "number") (Seq "(" (Rule "expr") ")" ))
  (handle-choice p_results
  ; handle-choice assign the current result to 'it'
  
    ; the first expression is called only if (Rule "number") matched.
    ; We simply return the result.
    it
    
    ; the second expression is call if (Seq "(" (Rule "expr") ")" ) matched
    ; we extract the result of (Rule "expr").
    (get it 1)))
```
