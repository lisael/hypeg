PYTHON=python
PYTEST=pytest

hy_modules=$(shell find . -name "*.hy")
test_modules=$(shell find tests -name "test_*.hy")

test:
	$(PYTEST) -s -v tests
	
print-%:  ## Print a Makefile variable (e.g. print-PYPI_SERVER)
	@echo $*=$($*)

clean:
	find . -name '__pycache__' -type d | xargs rm -rf
	find tests -name "test_*.py" -delete

clean-tests:
	rm -rf tests/__pycache__/
	
.PHONY: test clean
