(defmacro/g! handle-choice [result &rest expressions]
  (setv g!body
    `(do
       (setv g!idx (len ~result))))
  (setv g!cond `(cond))
  (.append g!body g!cond)
  (for [idx (range (len expressions))]
	(.append g!cond
      `[(= g!idx ~(+ idx 1))
        (setv it (get ~result ~idx))
        ~(get expressions idx)]))
  g!body)

(defmacro chosen [lst]
  `(first (filter (fn [x] (not (instance? NoMatch x))) ~lst)))

(defmacro raw [lst]
  `(.join "" (filter (fn [x] (instance? str x)) (flatten ~lst))))
