(require [hypeg.helpers [handle_choice chosen raw]])
(import [hy.models [HyDict]])
(import [hypeg.exceptions [NoMatch]])
; (import [ipdb [set_trace]])

(eval-and-compile
  (defclass ParserExpr []
    (defn --init-- [self]
      (setv self.p_debug_indent 0))
    (defn p-e-debug [self]
      (return self.__class__.__name__)))
  
  (defclass AnyChar [ParserExpr])

  (defclass KleenStar [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))

  (defclass OneOrMore [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))

  (defclass Seq [ParserExpr]
    (defn --init-- [self &rest subs]
      (setv self.subs subs)))

  (defclass Choice [ParserExpr]
    (defn --init-- [self &rest subs]
      (setv self.subs subs)))

  (defclass Literal [ParserExpr]
    (defn --init-- [self value]
      (setv self.value value))
    (defn p-e-debug [self]
      (return (+ self.__class__.__name__ (+ " " self.value)))))

  (defclass CharIn [ParserExpr]
    (defn --init-- [self value]
      (setv self.value value))
    (defn p-e-debug [self]
      (return (+ self.__class__.__name__ (+ " " self.value)))))

  (defclass CharNotIn [ParserExpr]
    (defn --init-- [self value]
      (setv self.value value))
    (defn p-e-debug [self]
      (return (+ self.__class__.__name__ (+ " " self.value)))))

  (defclass Not [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))

  (defclass Lookahead [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))

  (defclass Maybe [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))
  
  (defclass Discard [ParserExpr]
    (defn --init-- [self sub]
      (setv self.sub sub)))

  (defclass Rule [ParserExpr]
    (defn --init-- [self name]
      (setv self.name name))
    (defn p-e-debug [self]
      (return (+ self.__class__.__name__ (+ " " self.name))))))

(defmacro with-result-list [concat &rest body]
  (setv do-concat
    ; TODO: this should be computed at compile time
    (if (eval concat)
      `(if (all (map (fn [x] (instance? str x)) p-rl-current))
        (setv p-rl-current (.join "" p-rl-current)))
      `(do)))
  `(do
    (setv p-rl-current [])
    (.append p-rl-stack p-rl-current)
    (try
      ~@body
      (except [NoMatch]
        (.pop p-rl-stack)
        (setv p-rl-current (last p-rl-stack))
        (raise))
      (else
        (.pop p-rl-stack)
        ~do-concat
        (.append (last p-rl-stack) p-rl-current)
        (setv p-rl-current (last p-rl-stack))))))

(defmacro no-match []
  `(do
    (setv p-nm-exc (NoMatch self.p-pos))
    (setv self.p-pos (.pop p_pos_stack))
    (raise p-nm-exc)))

(defmacro match [result]
  `(do
    (.append p-rl-current ~result)
    (.pop p_pos_stack)))

(defmacro no-consume [&rest body]
  (setv startpos (gensym))
  `(do
     (setv ~startpos self.p-pos)
     ~@body
     (setv self.p-pos ~startpos)))

(defmacro discard-results [&rest body]
  (setv form `(with-result-list False
     ~@body))
  (.append form `(if p-rl-current (.pop p-rl-current)))
  form)

(defmacro debug-indent []
  `(* " "
     (-
      (+ self.p-debug-indent
        (len p-rl-stack))
      2)))

(defmacro/g! compile-expr [opts expr]
  (import [hy.models[HyKeyword]])

  ; Instanciate the expression
  (setv real-expr (eval expr))

  ; syntax sugar, strings are compiled as Literals
  (if (instance? str real-expr)
    (do
      (setv expr `(Literal ~expr))
      (setv real-expr (eval expr))))

  ; create the body of the expression
  (setv form
    `(do
      ~(+ (.p-e-debug real-expr) " ")
       (.append p-pos-stack self.p-pos)))

  ; parse options
  (setv opts (eval opts))
  (if (eval (.get opts (HyKeyword "debug")))
    ; add the debug output
    (do
      (.append form
        `(print
           (+
            (debug-indent)
            ~(.p-e-debug real-expr)
            "\n"
            (debug-indent) " > "
            (self.p-slice self.p-pos (+ self.p-pos 20)))))))

  ; add the expression code
  (.append form (cond

    [ (instance? AnyChar real-expr)
      ; Return anything or raise at EOF
      `(do
         (setv g!char ((. self p-next)))
         (if (is g!char None)
           (no-match)
           (match g!char)))]

    [ (instance? CharIn real-expr)
      (setv g!value real-expr.value)
      `(do
         (setv g!char ((. self p-next)))
         (if (or (is g!char None) (not (in g!char ~g!value)))
           (no-match))
         (match g!char))]

    [ (instance? CharNotIn real-expr)
      (setv g!value real-expr.value)
      `(do
         (setv g!char (self.p-next))
         (if (or (is g!char None) (in g!char ~g!value))
           (no-match))
         (match g!char))]
    
    [ (instance? Maybe real-expr)
      (setv sub (get expr 1))
      `(try
        (compile-expr ~opts ~sub)
        (except [g!exc NoMatch]
          (.append p-rl-current g!exc)))]

    [ (instance? Not real-expr)
      ; raise if it didn't raise, discard and don't consume otherwise
      (setv sub (get expr 1))
      `(no-consume
        (discard-results
          (try
            (compile-expr ~opts ~sub)
            (except [NoMatch]
              (.append p-rl-current ""))
            (else
              (raise (NoMatch self.p_pos))))))]

    [ (instance? Discard real-expr)
      (setv sub (get expr 1))
        `(discard-results
            (compile-expr ~opts ~sub))]

    [ (instance? Lookahead real-expr)
      (setv sub (get expr 1))
      `(no-consume
        (discard-results
          (compile-expr ~opts ~sub)))]

    [(instance? KleenStar real-expr)
     (setv sub (get expr 1))
     (setv pos (gensym))
     `(with-result-list True
        (setv ~pos self.p_pos)
        (try
         (while True
           (compile-expr ~opts ~sub)
           (if (= self.p_pos ~pos) (break) (setv ~pos self.p_pos)))
         (except [NoMatch]
           (do))))]

    [(instance? OneOrMore real-expr)
     (setv sub (get expr 1))
     (setv pos (gensym))
     `(with-result-list True
        (setv ~pos self.p_pos)
        (try
         (while True
           (compile-expr ~opts ~sub)
           (if (= self.p_pos ~pos) (break) (setv ~pos self.p_pos)))
         (except [NoMatch]
           (if (not p-rl-current) (raise)))))]

    [ (instance? Literal real-expr)
      (setv g!value real-expr.value)
      (setv g!len (len real-expr.value))
      `(do
         (setv g!starter (self.p-slice self.p_pos (+ self.p_pos ~g!len)))
         (if (= g!starter ~g!value)
           (do
             (+= self.p_pos ~g!len)
             (match g!starter))
           (do
             (no-match))))]

    [(instance? Seq real-expr)
     (setv expr-form `(with-result-list False))
     (for [sub (list (drop 1 expr))]
       (.append expr-form `(compile-expr ~opts ~sub)))
     expr-form]
                    
    [(instance? Rule real-expr)
      (setv rulename (mangle (get expr 1)))
      `(try
         (match ((getattr self ~rulename)))
         (except [NoMatch]
           (setv self.p-pos (.pop p_pos_stack))
           (raise)))]

    [(instance? Choice real-expr)
     (setv current-exc (gensym))
     (setv expr-form
       `(with-result-list False
         (setv ~current-exc (NoMatch -1))))
     (setv main-try `(do))
     (.append expr-form main-try)
     (setv current main-try)
     (for [sub (list (drop 1 expr))]
       (setv sub-try
         `(try
            (compile-expr ~opts ~sub)))
       (.append current sub-try)
       (setv current
        `(except [g!exc NoMatch]
           (if (> g!exc.pos (. ~current-exc pos))
             (setv ~current-exc g!exc))
           ; (.append p-pos-stack -1)
           (.append p-rl-current  g!exc)))
       (.append sub-try current))
       (.append expr-form 
        `(if (all (map (fn [x] (instance? NoMatch x)) p-rl-current))
           (raise (NoMatch self.p_pos :parents p-rl-current))))
       expr-form]

    [True (raise (ValueError real-expr))]))
   form)

(defmacro rule [name opts expr &rest handler]
  (import [hy.models[HyKeyword]])
  (setv handler_name (read-str(+ "on_" name)))

  ; parse options
  (setv opts (eval opts))
  (setv call-expr
    (if (eval (.get opts (HyKeyword "debug")))
     `(try
       (+= self.p-debug-indent 1)
       (compile-expr ~opts ~expr)
       (print
         (+ (debug-indent) "< " (str(get p-rl-current 0)) " >"))
       (except [Exception]
         (print
           (+ (debug-indent) "< no-match >"))
         (raise))
       (finally
         (-= self.p-debug-indent 1)))
     `(compile-expr ~opts ~expr)))

  (setv rule-func
    `(defn ~name [self]
      (setv p-rl-stack [])
      (setv p-rl-current [])
      (.append p-rl-stack p-rl-current)
      (setv p-pos-stack [self.p-pos])
      ~call-expr))
  (.append rule-func
    (if handler
      `((. self ~handler_name) (get p-rl-current 0) (self.p-slice (get p-pos-stack 0) self.p-pos))
      `(get p-rl-current 0)))
  (setv form `(do ~rule-func))
  (if handler
    (.append form
      `(defn ~handler_name [self p_results p_raw]
        (do ~@handler))))
  form)

(defmacro/g! defparser [name &rest rules]
  ; get the opts
  (import [hy.models[HyDict]])
  (if (instance? HyDict (first rules))
    (do
      (setv opts (first rules))
      (setv rules (list (drop 1 rules))))
    (setv opts (HyDict [])))
  ; injects the opts in rules
  (for [r rules]
    ; do not inject if the rules has its own opts
    (if (not (instance? HyDict (get r 2)))
        (.insert r 2 opts)))
  ; get the main entry point
  (setv entrypoint (HyString (get (get rules 0) 1)))
  ; create the class
  `(do
    (require [hypeg.parser [compile-expr match no-match with-result-list discard-results debug-indent]])
    (import [hypeg.parser [ParserBase NoMatch]])
    (defclass ~name [ParserBase]
      (defn --call-- [self src &optional [entrypoint ~entrypoint]]
        (setv entrypoint (getattr self (mangle entrypoint)))
        (setv self.p-debug-indent 0)
        (setv self.p-src src)
        (setv self.p-pos 0)
        (setv self.p-len (len src))
        (entrypoint))

      ~@rules)))

(defclass ParserBase []

  (defn p-slice [self start end]
    ((. self.p-src --getitem--) (slice start end)))

  (defn p-next [self]
    (return (if
      (= self.p_pos self.p-len)
      None
      (do
        (setv result (get self.p-src self.p_pos))
        (+= self.p_pos 1)
        result)))))

(defparser HyPEG {:debug True}
  ; grammar <- rule*
  (rule grammar (Seq (Rule "ig") (KleenStar (Rule "rule")))
    (do (print p-results)
    (+ [:grammar] (get p-results 1))))

  ; identifier <- [A-Za-z_][A-Za-z0-9_]*
  (rule identifier
    (Seq
      (CharIn "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_")
      (KleenStar (CharIn "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-")))
    p-raw)
  
  ; charsetchar <- string-escape / "\\" "]"/ [^\]]
  (rule charset-char
    (Choice (Rule "string-escape") (Seq "\\" "]") (CharNotIn "]"))
    (handle-choice p-results
      it
      "]"
      it))

  ; charset-range <- charsetchar "-" charsetchar
  (rule charset-range
    (Seq (Rule "charset-char") "-" (Rule "charset-char"))
    (.join "" (list (map chr (range (ord (get p-results 0)) (+ 1 (ord (get p-results 2))))))))

  ; charset <- "[" "^"? ( charset-range / charsetchar )+ "]"
  (rule charset
    (Seq "[" (Maybe "^") (OneOrMore (Choice (Rule "charset-range") (Rule "charset-char"))) "]")
    (do #_(print ( + ">>> " (str p-results)))
    [:charset (instance? NoMatch (get p-results 1)) (raw (get p-results 2))]))

  ; stringlit <- "\"" ( string-escape / doublequote-escape)* "\"" / "'" ("\\" . / [^'])* "'"
  (rule stringlit
    (Choice
      (Seq "\"" (KleenStar (Choice (Rule "string-escape") (CharNotIn "\""))) "\"")
      (Seq "'" (KleenStar (Choice (Rule "string-escape") (CharNotIn "'"))) "'"))
    [:literal (raw (get (chosen p-results) 1))])

  ; string-escape <- "\\" ( [rnt\\] / [0-9a-zA-Z]+)
  (rule string-escape
    (Seq "\\" (Choice (CharIn "rnt\\") (OneOrMore (CharIn "0123456789abcdefABCDEF"))))
    (handle-choice (get p-results 1)
      (cond
        [(= it "\\") "\\"]
        [(= it "r") "\r"]
        [(= it "t") "\t"]
        [(= it "n") "\n"])
      (int (+ "0x" it) 16)))

  ; builtin <- "\\" identifier / [^\13\10]
  (rule macro_
    (Seq "\\" (Rule "identifier"))
    [:macro (get p_results 1)])

  ; comment <- ';' @ \n
  (rule comment
    (Discard (Seq ";" (KleenStar (CharNotIn "\n")) (Choice (CharIn "\n") (Not(AnyChar))))))

  ; ig <- (\s / comment)* ; things to ignore
  (rule ig
    (Discard (KleenStar (Choice (CharIn " \t\r\n") (Rule "comment")))))
  
  ; rule <- identifier \s* "<-" expr ig
  (rule rule
    (Seq (Rule "identifier") (KleenStar (CharIn " \t\r\n")) "<-" (Rule "expr") (Rule "ig"))
    [:rule (get p_results 0) (get p_results 3)]
  )

  ; ident-no-arrow <- identifier !(\s* "<-")
  (rule ident-no-arrow
    (Seq (Rule "identifier") (Not (Seq (KleenStar (CharIn " \t\r\n")) "<-" )))
    (get p_results 0))
  
  ; prefix-opr <- ig ('&' / '!' / '_' / '@')
  (rule prefix-opr
    (Seq (Rule "ig") (Choice "&" "!" "_" "@"))
    (handle-choice (get p_results 1)
     [:lookahead]
     [:not]
     [:discard]
     [:until]))

  ; literal <- ig ( ident-no-arrow / charset / stringlit / builtin / '.' / "(" expr ig ")" )
  (rule literal
    (Seq (Rule "ig") (Choice
      (Rule "ident-no-arrow")
      (Rule "charset")
      (Rule "stringlit")
      (Rule "macro_")
      "."
      (Seq "(" (Rule "expr") (Rule "ig") ")")))
    (handle-choice (get p_results 1)
      [:call it]
      it
      it
      it
      [:anychar]
      (get it 1)))

  ; postfix-opr <- ig ('?' / '*' / '+')
  (rule postfix-opr
    (Seq (Rule "ig") (Choice "?" "*" "+"))
    (handle-choice (get p_results 1)
     [:maybe]
     [:some]
     [:many]))

  ; primary <- prefix-opr* (literal postfix-opr*)
  (rule primary
    (Seq (KleenStar (Rule "prefix-opr")) (Seq (Rule "literal") (KleenStar (Rule "postfix-opr"))))
    (setv expr (get (get p_results 1) 0))
    (for [postfix (get (get p_results 1) 1)]
      (.append postfix expr)
      (setv expr postfix))
    (for [prefix (reversed (get p_results 0))]
      (.append prefix expr)
      (setv expr prefix))
    expr)

  ; seq-expr <- primary+
  (rule seq-expr
    (OneOrMore (Rule "primary"))
    (if (> (len p_results) 1)
      (+ [:seq] p_results)
      (get p_results 0)))

  ; expr <- seq-expr (ig "/" expr)*
  (rule expr
    (Seq (Rule "seq-expr") (Maybe (Seq (Rule "ig") "/" (Rule "expr"))))
    (setv rest (get p_results 1))
    ; (set_trace)
    (if (not (instance? NoMatch rest))
      (do
        (setv choices (get rest 2))
        (if (not (= :choice (get choices 0)))
          (do (setv choices [:choice choices])))
        ; (print choices)
        ; (print)
        (.insert choices 1 (get p_results 0))
        choices)
      (get p_results 0)))
)

; ((HyPEG) "; hop" "comment")

(setv gr "expr <- sum
    sum <- product ([+-] product)*
    product <- value ([*/] value)*
    value <- number / '(' expr ')'
    number <- [0-9]+")

(print ((HyPEG) gr))

; (defmacro grammar [gr]
;   ((HyPEG) gr))
