class NoMatch(Exception):
    def __init__(self, pos=-1, parents=None, *args, **kwargs):
        self.pos = pos
        self.parents = parents

    def __eq__(self, other):
        return isinstance(other, type(self)) and self.pos == other.pos
