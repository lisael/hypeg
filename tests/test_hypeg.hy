(require [hypeg.parser [defparser rule]])
(require [hypeg.helpers [handle_choice chosen raw]])
(import [hypeg.parser [HyPEG]])

(defmacro test-rule [rulename src expected]
  (setv debug False)
  (setv debug True)
  (setv form
    `(do
       (setv result ((HyPEG) ~src ~rulename))))
  (if debug
    (.append form
      `(do
        (print "+++++++++++++++++++++++")
        (print result)
        (print "+++++++++++++++++++++++"))))
  (.append form
    `(assert (= result ~expected)))
  form)

(defn test_charset []
  (test-rule "charset_range" "a-f" "abcdef")
  (test-rule "charset" "[a-f]" [:charset True "abcdef"])
  (test-rule "charset" "[abc]" [:charset True "abc"])
  (test-rule "charset" "[^a-f]" [:charset False "abcdef"])
  (test-rule "charset" "[a-f^-]" [:charset True "abcdef^-"])
  (test-rule "charset" "[a-f\\]]" [:charset True "abcdef]"])
)

(defn test_stringlit []
  (test-rule "stringlit" "\"hop\"" [:literal "hop"])
  (test-rule "stringlit" "'hip'" [:literal "hip"])
  (test-rule "stringlit" "'123\\t456'" [:literal "123\t456"])
)

(defn test_comment []
  (test-rule "comment" "; hop" [])
)

(defn test_primary []
  (test-rule "primary" "[a-f]*" [:some [:charset True "abcdef"]])
  (test-rule "primary" "!'b'*" [:not [:some [:literal "b"]]])
  (test-rule "primary" "_@'\\n'" [:discard [:until [:literal "\n"]]])
)

(defn test-seq-expr []
  (test-rule "seq-expr" "'hello'" [:literal "hello"])
  (test-rule "seq-expr" "'hello' 'world'" [:seq [:literal "hello"] [:literal "world"]])
)

(defn test-expr []
  (test-rule "expr" "'hello'" [:literal "hello"])
  (test-rule "expr" "'hello' 'world'" [:seq [:literal "hello"] [:literal "world"]])
  (test-rule "expr" "'hello' / 'world'" [:choice [:literal "hello"] [:literal "world"]])
  (test-rule "expr" "'hello' / 'hi' / 'world'" [:choice [:literal "hello"] [:literal "hi"] [:literal "world"]])
  (test-rule "expr" "('hello' / 'hi') 'world'" [:seq [:choice [:literal "hello"] [:literal "hi"]] [:literal "world"]])
)

(defn test-rule_ []
  (test-rule "rule" "hop <- 'hi'" [:rule "hop" [:literal "hi"]])
)

(defn test-grammar []
  (test-rule "grammar" " hop <- 'hi'\n hip <- hello"
    [:grammar
      [:rule "hop" [:literal "hi"]]
      [:rule "hip" [:call "hello"]]])
)
