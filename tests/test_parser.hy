(require [hypeg.parser [defparser rule]])
(require [hypeg.helpers [handle-choice raw]])
(import pytest)

(defn check_parser [parser src expected &optional [expected_pos None]]
  (setv p (parser))
  (if (instance? NoMatch expected)
    (try
      (print "try...")
      (p src)
      (except [exc NoMatch]
        (print "except...")
        (print exc.pos)
        (setv result exc)))
    (setv result (p src)))
  (print result)
  (assert (= result expected))
  (if (not (is None expected_pos))
    (do (print (+ "pos: " (str p.p_pos)))
    (assert (= p.p_pos expected_pos)))))

(defparser Lit1 {:debug False}
  (rule Lit (Literal "hello")
    p_results))

(defparser Any1 {:debug False}
  (rule main (AnyChar)
    p_results))

(defn test_literals []
  (assert (= ((Lit1) "hello") "hello"))
)

(defparser Seq1 {:debug False}
  (rule hop (Seq "a" "b" "c")(do (print p-results) p-results)))

(defn test_seq []
  (assert (= ((Seq1) "abc") ["a" "b" "c"]))
)

(defparser Choice1 {:debug True}
  (rule hop (Choice "a" "b") p_results))

(defn test_choice []
  ; (check_parser Choice1 "a" ["a"] 1)
  ; (check_parser Choice1 "b" [(NoMatch 0) "b"] 1)
  (check_parser Choice1 "c" (NoMatch 0) 0)
)

(defparser OneOrMore1 {:debug False}
  (rule hop (OneOrMore "a")(do (print p-results) p-results)))

(defparser OneOrMore2 {:debug False}
  (rule hop (OneOrMore (Choice "a" "b"))(do (print p-results) p-results)))

(defn test_one_or_more []
  (check_parser OneOrMore1 "aa" "aa" 2)
  (check_parser OneOrMore2 "ab" [["a"] [(NoMatch 1) "b"]])
  (check_parser OneOrMore2 "abc" [["a"] [(NoMatch 1) "b"]])
)

(defparser Kleen1 {:debug False}
  (rule hop (KleenStar (CharNotIn "\n"))(do (print p-results) p-results)))

(defn test-kleen-star []
  (check_parser Kleen1 "aab" "aab" 3)
)

(defparser Discard1 {:debug False}
  (rule hop (Discard "a") (do p-results)))

(defn test-discard []
  (check_parser Discard1 "a" [] 1)
)
